import 'package:flutter/material.dart';

enum Categories {
  vegetables,
  fruit,
  meat,
  dairy,
  carbs,
  sweets,
  spices,
  convenience,
  hygiene,
  other
}

// ignore: camel_case_types
class Category {
  const Category(this.title, this.color);

  final String title;
  final Color color;
}
