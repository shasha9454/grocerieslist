import 'package:flutter/material.dart';
import 'package:shopping_list/categories.dart';
import 'package:shopping_list/newItem.dart';
import 'package:shopping_list/GroceryItem.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class GroceryList extends StatefulWidget {
  const GroceryList({super.key});

  @override
  State<GroceryList> createState() => _GroceryListState();
}

class _GroceryListState extends State<GroceryList> {
  late Future<List<Grocery_Item>> _loadedItems;

  List<Grocery_Item> _groceriesItems = [];
  var isLoading = true;
  String? error;

  @override
  void initState() {
    super.initState();
    _loadedItems = _loadItem();
  }

  void onRemove(Grocery_Item groceryItem) async {
    final index = _groceriesItems.indexOf(groceryItem);

    setState(() {
      _groceriesItems.remove(groceryItem);
    });

    final url = Uri.https('flutter-90d09-default-rtdb.firebaseio.com',
        'shopping-List/${groceryItem.id}.json');
    final response = await http.delete(url);

    if (response.statusCode >= 400) {
      setState(() {
        _groceriesItems.insert(index, groceryItem);
      });
    }
  }

  Future<List<Grocery_Item>> _loadItem() async {
    final url = Uri.https(
        'flutter-90d09-default-rtdb.firebaseio.com', 'shopping-List.json');

    final response = await http.get(url);
    if (response.statusCode >= 400) {
      throw Exception('Failed to fetch Data , Please try again later!');
      // in condition of error
    }
    if (response.body == 'null') {
      return [];
    }
    final Map<String, dynamic> listData = json.decode(response.body);
    final List<Grocery_Item> storeData = [];

    for (final item in listData.entries) {
      final category = categories.entries
          .firstWhere(
            (element) => element.value.title == item.value['category'],
          )
          .value;
      storeData.add(
        Grocery_Item(
            id: item.key,
            name: item.value['name'],
            quantity: item.value['quantity'],
            category: category),
      );
    }
    // setState(() {
    //   _groceriesItems = storeData;
    //   isLoading = false;
    // });
    return storeData;
  }
  // for initialize the method called initSate method for invoke the method initially

  // ignore: non_constant_identifier_names

  void _onSelect() async {
    final resultData = await Navigator.of(context).push<Grocery_Item>(
        MaterialPageRoute(builder: (ctx) => const NewItem()));
    if (resultData == null) {
      return;
    } else {
      _groceriesItems.add(resultData);
    }
    // if (result == null) {
    //   return;
    // } else {
    //   setState(() {
    //     Grocery_List.add(result);
    //   });
  }

  @override
  Widget build(BuildContext context) {
    // Widget content =
    // if (isLoading) {
    //   content = const Center(
    //     child: CircularProgressIndicator(),
    //   );
    // if (error != null) {
    //   content = Center(
    //     child: Text(error!),
    //   );
    // }

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Groceries-List ...",
        ),
        actions: [
          IconButton(onPressed: _onSelect, icon: const Icon(Icons.add))
        ],
      ),
      body: FutureBuilder(
          future: _loadedItems,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (snapshot.hasError) {
              return Center(
                child: Text(snapshot.error.toString()),
              );
            }
            if (snapshot.data!.isEmpty) {
              return const Center(
                child: Text("No Item Available!"),
              );
            }
            return ListView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (ctx, index) => Dismissible(
                onDismissed: (direction) => onRemove,
                key: ValueKey(snapshot.data![index].id),
                child: ListTile(
                  title: Text(snapshot.data![index].name),
                  leading: Container(
                    height: 12,
                    width: 20,
                    color: snapshot.data![index].category.color,
                  ),
                  trailing: Text(snapshot.data![index].quantity.toString()),
                ),
              ),
            );
          }),
    );
  }
}
