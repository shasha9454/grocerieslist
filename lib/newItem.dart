import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shopping_list/GroceryItem.dart';
import 'package:shopping_list/categories.dart';
import 'package:shopping_list/catogory.dart';
import 'package:http/http.dart' as http;

class NewItem extends StatefulWidget {
  const NewItem({super.key});

  @override
  State<StatefulWidget> createState() {
    return _NewItemState();
  }
}

class _NewItemState extends State<NewItem> {
  var isSending = false;
  final valueKey = GlobalKey<FormState>();

  var enteredValue = "";
  // ignore: non_constant_identifier_names
  var Initialvalue = 1;
  var category = categories[Categories.spices]!;

  void _display() async {
    if (valueKey.currentState!.validate()) {
      valueKey.currentState!.save();
      setState(() {
        isSending = true;
      });

      final url = Uri.https(
          'flutter-90d09-default-rtdb.firebaseio.com', 'shopping-List.json');

      final response = await http.post(url,
          headers: {
            'Content-type': 'application/json',
          },
          body: json.encode({
            'name': enteredValue,
            'quantity': Initialvalue,
            'category': category.title
          }));

      // Navigator.of(context).pop(Grocery_Item(
      //     id: DateTime.now().toString(),
      //    ));
      // ignore: avoid_print
      print(response.statusCode);
      if (!context.mounted) {
        return;
      }

      final inputQuick = json.decode(response.body);
      // ignore: use_build_context_synchronously
      Navigator.of(context).pop(Grocery_Item(
          id: inputQuick['name'],
          name: enteredValue,
          quantity: Initialvalue,
          category: category));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("The Form "),
      ),
      body: Padding(
        padding: const EdgeInsets.all(23),
        child: Form(
          key: valueKey,
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      maxLength: 50,
                      decoration: const InputDecoration(label: Text('Name')),
                      validator: ((value) {
                        if (value == null ||
                            value.trim().isEmpty ||
                            value.trim().length <= 1 ||
                            value.trim().length > 50) {
                          return "The Required Value Should in between 1 to 50 ";
                        }
                        return null;
                      }),
                      onSaved: (newValue) {
                        if (newValue == null) {
                          return;
                        }
                        enteredValue = newValue;
                      },
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          label: Text("Quantity"),
                        ),
                        initialValue: Initialvalue.toString(),
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              int.tryParse(value) == null ||
                              int.tryParse(value)! <= 0) {
                            return "Must be a valid Positive Number ! ";
                          }
                          return null;
                        },
                        onSaved: (value) {
                          Initialvalue = int.parse(
                              value!); // parse throw an error and tryParse throw an Null value..
                        },
                      ),
                    ),
                    const SizedBox(
                      width: 6,
                    ),
                    Expanded(
                      child: DropdownButtonFormField(
                          value: category,
                          items: [
                            for (final category in categories
                                .entries) // entries used for iterating any map value ...
                              DropdownMenuItem(
                                value: category.value,
                                child: Row(
                                  children: [
                                    Container(
                                      height: 15,
                                      width: 15,
                                      color: category.value.color,
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Text(category.value.title),
                                  ],
                                ),
                              ),
                          ],
                          onChanged: ((value) {
                            setState(() {
                              category = value!;
                            });
// here we No need of Onsaved Method ...
                          })),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () {
                        if (valueKey.currentState != null) {
                          isSending ? null : valueKey.currentState!.reset();
                        }
                      },
                      child: const Text("Reset"),
                    ),
                    const SizedBox(
                      width: 7,
                    ),
                    ElevatedButton(
                      onPressed: isSending ? null : _display,
                      child: isSending
                          ? const SizedBox(
                              height: 16,
                              width: 16,
                              child: CircularProgressIndicator(),
                            )
                          : const Text("submit"),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
