import 'package:shopping_list/catogory.dart';

// ignore: camel_case_types
class Grocery_Item {
  const Grocery_Item({
    required this.id,
    required this.name,
    required this.quantity,
    required this.category,
  });
  final String id;
  final String name;
  final int quantity;
  final Category category;
}
