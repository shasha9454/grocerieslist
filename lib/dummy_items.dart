import 'package:shopping_list/GroceryItem.dart';
import 'package:shopping_list/catogory.dart';
import 'package:shopping_list/categories.dart';

final groceryItems = [
  Grocery_Item(
      id: 'a',
      name: 'Milk',
      quantity: 1,
      category: categories[Categories.dairy]!),
  Grocery_Item(
      id: 'b',
      name: 'Bananas',
      quantity: 5,
      category: categories[Categories.fruit]!),
  Grocery_Item(
      id: 'c',
      name: 'Beef Steak',
      quantity: 1,
      category: categories[Categories.meat]!),
];
